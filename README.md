# GlitchDM Fauux
A heavily modified version of <https://fauux.neocities.org/login.html> to work as an actual Linux login screen. Depends on [Antergros/web-greeter](https://github.com/Antergos/web-greeter). I am not in any way affiliated with fauux.

## Installation

1. Install [web-greeter](https://github.com/Antergos/web-greeter).
2. `sudo mkdir -p /etc/lightdm/lightdm.conf.d`
2. Create the file `/etc/lightdm/lightdm.conf.d/50-lightdm-webkit2-greeter.conf` and put this in it:
```ini
[Seat:*]
greeter-session=lightdm-webkit2-greeter
```
3. `git clone https://banepwn@bitbucket.org/banepwn/glitchdm-fauux.git`
4. `sudo cp -r glitchdm-fauux /usr/share/lightdm-webkit/themes`
5. Edit the file `/etc/lightdm/lightdm-webkit2-greeter.conf` and set the `webkit-theme` property to `glitchdm-fauux`.
6. Reboot.

**Note: If you are using Ubuntu 17.10 or later, you are using GDM and not LightDM by default.** Here are the instructions for rectifying that.

1. `sudo apt install lightdm`
2. If the configuration screen does not appear or you already have LightDM installed, `sudo dpkg-reconfigure lightdm`
3. Select `lightdm` from the configuration screen.
4. Reboot.

## Keyboard controls

* Ctrl+R: **R**eload page
* Ctrl+M: Start/stop **m**usic
* Alt+H: **H**ibernate
* Alt+P: Sus**p**end
* Alt+R: **R**estart
* Alt+D: Shut**d**own

## Attribution

* `assets/arrow.png`: Original by me, CC0 1.0 Universal
* `assets/*`: Obtained from <https://fauux.neocities.org>, unknown/no license
* `favicon_*px.png`: Obtained from <https://fauux.neocities.org>, unknown/no license
* `index.html`: Heavily modified from <https://fauux.neocities.org/login.html>
* `login.css`: Heavily modified from <https://fauux.neocities.org/stylesheetLogin.css>
* `scripts/main.js`: Original by me, CC0 1.0 Universal
* `scripts/mock.js`: Slightly modified from <https://github.com/pedropenna/musfealle/blob/master/js/mock.js>, GNU General Public License Version 3 **(not included in repository, get from <https://pastebin.com/J8r7JJP7>)**

**The CC0 license in LICENSE.txt only applies to `scripts/main.js` and `assets/arrow.png`!**
