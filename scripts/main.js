var show_error;
var show_prompt;
var authentication_complete;
window.onload = function() {
	"use strict";
	
	var subtitle = document.getElementById("subtitle");
	
	var usernameField = document.getElementById("username");
	var passwordField = document.getElementById("password");
	var sessionField = document.getElementById("session");
	var submit = document.getElementById("submit");
	var form = document.querySelector("form");
	
	var popup = document.getElementById("popup");
	var popupText = document.getElementById("popupText");
	var popupButton = document.getElementById("popupButton");
	//var popupConfirm = document.getElementById("popupButtonAlt");
	
	var blanker = document.getElementById("blanker");
	var blocker = document.getElementById("blocker");
	
	var music = document.getElementById("music");
	
	var sleeping = false;
	var secondsSinceLastActivity = 0;
	function registerActivity() {
		secondsSinceLastActivity = 0;
		if (sleeping) {
			sleeping = false;
			blanker.dataset.hidden = true;
		};
	};
	["mousedown", "mousemove", "keydown", "scroll", "touchstart"].forEach(eventName => {
		document.addEventListener(eventName, registerActivity, true);
	});
	setInterval(function() {
		secondsSinceLastActivity++;
		if (!sleeping && secondsSinceLastActivity > 60) {
			sleeping = true;
			blanker.dataset.hidden = false;
		};
	}, 1000);
	
	popupButton.addEventListener("click", function() {
		blocker.dataset.hidden = true;
		popup.dataset.hidden = true;
	});
	
	var sessionIndex = {};
	lightdm.sessions.forEach((session, index) => {
		sessionIndex[session.key] = index;
		sessionField.options[index] = new Option(session.name, index);
	});
	
	setInterval(function() {
		var date = new Date();
		subtitle.textContent = date.toLocaleString();
	}, 1000);
	
	usernameField.focus();
	lightdm.users.forEach(user => {
		if (user.logged_in) {
			usernameField.value = user.name;
			passwordField.focus();
			sessionField.value = sessionIndex[user.session];
			return;
		}
	});
	
	form.addEventListener("submit", function(event) {
		event.preventDefault();
		if (usernameField.value) {
			usernameField.disabled = true;
			passwordField.disabled = true;
			sessionField.disabled = true;
			submit.disabled = true;
			lightdm.cancel_timed_login();
			lightdm.start_authentication(usernameField.value);
		} else {
			show_error("You need to specify your user ID.");
		}
	});
	
	document.onkeydown = function() {
		if (!event.shiftKey && !event.metaKey) {
			if (event.ctrlKey && !event.altKey) {
				switch (event.key) {
					case "r":
						location.reload();
						break;
					case "m":
						if (music.dataset.playing === "true") {
							music.dataset.playing = "false";
							music.pause();
							music.src = "";
						} else {
							music.dataset.playing = "true";
							music.src = music.dataset.src;
							music.load();
							music.play();
						}
						break;
				}
			} else if (!event.ctrlKey && event.altKey) {
				switch (event.key) {
					case "h":
						lightdm.hibernate();
						break;
					case "p":
						lightdm.suspend();
						break;
					case "r":
						lightdm.restart();
						break;
					case "d":
						lightdm.shutdown();
						break;
				}
			}
		}
	};
	
	// Callbacks
	show_error = function(message) {
		console.error(message);
		blocker.dataset.hidden = false;
		popup.dataset.hidden = false;
		popupText.textContent = message || "Unknown error.";
		usernameField.disabled = false;
		passwordField.disabled = false;
		sessionField.disabled = false;
		submit.disabled = false;
	}
	
	show_prompt = function(message) {
		if (message == "Password: ") {
			lightdm.provide_secret(passwordField.value);
		} else {
			var error = "Unexpected prompt: " + message;
			show_error(error);
			throw error;
			// Throwing SHOULD cause web-greeter to allow the user to
			// switch to a different theme, but in my test nothing
			// happened.
		}
	}
	
	authentication_complete = function() {
		if (lightdm.is_authenticated) {
			var session = lightdm.sessions[sessionField.selectedOptions[0].value || 0];
			console.log("Successful authentication with session %q!", session.name);
			lightdm.login(lightdm.authentication_user, session.key);
		} else {
			show_error("Incorrect username or password.");
		}
	}
}
